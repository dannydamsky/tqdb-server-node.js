"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MainResource {
    constructor(app, db) {
        this.app = app;
        this.db = db;
    }
    createRouting() {
        this.getWelcomePage();
        this.postWelcomePage();
    }
    getWelcomePage() {
        this.app.get('/', (request, response) => {
            response.sendFile("/index.html");
        });
    }
    postWelcomePage() {
        this.app.post('/', (request, response) => {
            response.sendfile("/index.html");
        });
    }
}
exports.MainResource = MainResource;
//# sourceMappingURL=mainresource.js.map