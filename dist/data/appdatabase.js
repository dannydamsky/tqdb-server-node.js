"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelizeconfig_1 = require("./config/sequelizeconfig");
const db = sequelizeconfig_1.createModels();
db.sequelize.sync();
exports.default = db;
//# sourceMappingURL=appdatabase.js.map