"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
;
exports.QuestionFactory = (sequelize, dataTypes) => {
    const attributes = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        question: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        realAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        firstFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        secondFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        thirdFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        confirmed: {
            type: dataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    };
    const Question = sequelize.define('Question', attributes);
    return Question;
};
//# sourceMappingURL=question.js.map