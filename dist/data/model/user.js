"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
;
exports.UserFactory = (sequelize, dataTypes) => {
    const attributes = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        apiKey: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        email: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        username: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        }
    };
    const User = sequelize.define('User', attributes);
    User.associate = models => {
        User.hasMany(models.Game);
    };
    return User;
};
//# sourceMappingURL=user.js.map