"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
exports.GameFactory = (sequelize, dataTypes) => {
    const attributes = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        score: {
            type: dataTypes.TINYINT,
            allowNull: false,
            defaultValue: 0
        },
        when: {
            type: dataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0
        }
    };
    const Game = sequelize.define('Game', attributes);
    return Game;
};
//# sourceMappingURL=game.js.map