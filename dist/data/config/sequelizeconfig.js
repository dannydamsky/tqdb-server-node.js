"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const user_1 = require("../model/user");
const question_1 = require("../model/question");
const game_1 = require("../model/game");
exports.createModels = () => {
    const sequelize = new Sequelize("tqdb", "root", "_Root123root", {
        host: 'localhost',
        dialect: 'mysql',
        port: 3306,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        operatorsAliases: false
    });
    const db = {
        sequelize,
        Sequelize,
        User: user_1.UserFactory(sequelize, Sequelize),
        Question: question_1.QuestionFactory(sequelize, Sequelize),
        Game: game_1.GameFactory(sequelize, Sequelize)
    };
    Object.values(db).forEach((model) => {
        if (model.associate) {
            model.associate(db);
        }
    });
    return db;
};
//# sourceMappingURL=sequelizeconfig.js.map