"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appdatabase_1 = require("./data/appdatabase");
const express = require("express");
const mainresource_1 = require("./resource/mainresource");
const app = express();
app.use(express.static(__dirname + "/../view"));
app.use(express.static(__dirname + "/../script"));
app.use(express.static(__dirname + "/../image"));
const mainResource = new mainresource_1.MainResource(app, appdatabase_1.default);
mainResource.createRouting();
const port = process.env.PORT || 3000;
console.log(`Listening on port ${port}...`);
app.listen(port);
//# sourceMappingURL=index.js.map