import { Random } from "./random";

export class AuthUtils {

    private static KEY_LENGTH: number = 32;

    private static ALL_CHARS: string[] = [
        // a-z
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        // A-Z
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        // 0-9
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ];

    private static ALL_CHARS_LENGTH: number = 62;

    static generateKey(): string {
        var apiKey: string[] = [];
        const random: Random = new Random();
        for (let i = 0; i < AuthUtils.KEY_LENGTH; i++) {
            apiKey.push(AuthUtils.ALL_CHARS[random.nextInt(AuthUtils.ALL_CHARS_LENGTH)]);
        }
        return apiKey.join("");
    }

    private constructor() {
    }
}