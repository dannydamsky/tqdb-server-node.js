import { Response } from 'express';
export class ErrorDetails<T> {

    static success<T>(item: T): ErrorDetails<T> {
        return new ErrorDetails<T>(200, "Success", item);
    }

    private statusCode: number;
    private message: string;
    private object: T;

    constructor(statusCode: number, message: string, item: T) {
        this.statusCode = statusCode;
        this.message = message;
        this.object = item;
    }

    send(response: Response): void {
        response.status(this.statusCode).send(this);
    }

    
}