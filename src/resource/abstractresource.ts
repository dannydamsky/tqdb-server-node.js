import { DbInterface } from "../data/typings/dbinterface";
import { Express } from 'express';

export abstract class AbstractResource {
    protected app: Express;
    protected db: DbInterface;

    constructor(app: Express, db: DbInterface) {
        this.app = app;
        this.db = db;
    }

    abstract createRouting(): void;
}