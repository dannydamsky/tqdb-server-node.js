import { AbstractResource } from "./abstractresource";
import { questionSchema, userSchema } from "../schema/schema";
import * as Joi from "joi";
import { ErrorDetails } from "../errordetails/errordetails";
import { Response } from "express";
import { QuestionInstance } from "../data/model/question";
import { AuthUtils } from "../util/authutils";
import { UserInstance } from "../data/model/user";

export class PostResource extends AbstractResource {
    createRouting(): void {
        this.postQuestion();
        this.postUser();
    }

    private postQuestion(): void {
        this.app.post('/post/question', (request, response) => {
            request.accepts('application/json');
            Joi.validate(request.body, questionSchema).then((requestBody) => {
                this.onQuestionInputValid(requestBody, response);
            }).catch((error) => {
                new ErrorDetails(400, error.details[0].message, null).send(response);
            });
        });
    }

    private onQuestionInputValid(requestBody: any, response: Response): void {
        this.db.Question.upsert(requestBody).then((success) => {
            this.onQuestionInsertComplete(requestBody, response, success);
        }).catch((error) => {
            new ErrorDetails(500, "Error: Unable to complete insert transaction.", error).send(response);
        });
    }

    private onQuestionInsertComplete(requestBody: any, response: Response, success: boolean): void {
        this.db.Question.findOne({
            where: requestBody
        }).then((question) => {
            this.onQuestionFound(response, success, question);
        }).catch((error) => {
            if (success) {
                new ErrorDetails(201, "Item inserted successfully, unable to return item.", error).send(response);
            } else {
                new ErrorDetails(200, "Item already exists in the database, unable to return item.", error).send(response);
            }
        });
    }

    private onQuestionFound(response: Response, success: boolean, question: QuestionInstance | null): void {
        if (success) {
            new ErrorDetails(201, "Item inserted successfully.", question).send(response);
        } else {
            new ErrorDetails(200, "Item already exists in the database", question).send(response);
        }
    }

    private postUser(): void {
        this.app.post('/post/user', (request, response) => {
            request.accepts('application/json');
            Joi.validate(request.body, userSchema).then((requestBody) => {
                this.onUserInputValid(requestBody, response);
            }).catch((error) => {
                new ErrorDetails(400, error.details[0].message, null).send(response);
            });
        })
    }

    private onUserInputValid(requestBody: any, response: Response): void {
        requestBody.apiKey = AuthUtils.generateKey();
        this.db.User.upsert(requestBody).then((success) => {
            this.onUserInsertComplete(requestBody, response, success);
        }).catch((error) => {
            new ErrorDetails(500, "Error: Unable to complete insert transaction.", error).send(response);
        });
    }

    private onUserInsertComplete(requestBody: any, response: Response, success: boolean): void {
        delete requestBody.apiKey;
        this.db.User.findOne({
            where: requestBody
        }).then((user) => {
            this.onUserFound(response, success, user);
        }).catch((error) => {
            if (success) {
                new ErrorDetails(201, "Item inserted successfully, unable to return item.", error).send(response);
            } else {
                new ErrorDetails(200, "Item already exists in the database, unable to return item.", error).send(response);
            }
        })
    }

    private onUserFound(response: Response, success: Boolean, user: UserInstance | null): void {
        if (success) {
            new ErrorDetails(201, "Item inserted successfully.", user).send(response);
        } else {
            new ErrorDetails(200, "Item already exists in the database", user).send(response);
        }
    }
}