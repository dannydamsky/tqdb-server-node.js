import * as Sequelize from "sequelize";
import { UserInstance, UserAttributes } from "../model/user";
import { QuestionInstance, QuestionAttributes } from "../model/question";
import { GameInstance, GameAttributes } from "../model/game";

export interface DbInterface {
  sequelize: Sequelize.Sequelize;
  Sequelize: Sequelize.SequelizeStatic;
  User: Sequelize.Model<UserInstance, UserAttributes>;
  Question: Sequelize.Model<QuestionInstance, QuestionAttributes>;
  Game: Sequelize.Model<GameInstance, GameAttributes>;
}