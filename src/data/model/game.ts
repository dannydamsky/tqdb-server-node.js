import * as Sequelize from "sequelize";
import { SequelizeAttributes } from "../typings/sequelizeattrs";
import { UserInstance, UserAttributes } from "./user";

export interface GameAttributes {
    id?: number;
    score: number;
    when: number;
};

export interface GameInstance extends Sequelize.Instance<GameAttributes>, GameAttributes {
}

export const GameFactory = (sequelize: Sequelize.Sequelize, dataTypes: Sequelize.DataTypes): Sequelize.Model<GameInstance, GameAttributes> => {
    const attributes: SequelizeAttributes<GameAttributes> = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        score: {
            type: dataTypes.TINYINT,
            allowNull: false,
            defaultValue: 0
        },
        when: {
            type: dataTypes.BIGINT,
            allowNull: false,
            defaultValue: 0
        }
    };

    const Game = sequelize.define<GameInstance, GameAttributes>('Game', attributes);
    return Game;
}