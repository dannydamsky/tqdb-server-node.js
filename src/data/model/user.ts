import * as Sequelize from "sequelize";
import { SequelizeAttributes } from "../typings/sequelizeattrs";
import { GameInstance, GameAttributes } from "./game";

export interface UserAttributes {
    id?: number;
    apiKey: string;
    email: string;
    username: string;
    password: string;
    games?: GameAttributes[] | GameAttributes['id'][];
};

export interface UserInstance extends Sequelize.Instance<UserAttributes>, UserAttributes {
    getGames: Sequelize.HasManyGetAssociationsMixin<GameInstance>;
    setGames: Sequelize.HasManySetAssociationsMixin<GameInstance, GameInstance['id']>;
    addGames: Sequelize.HasManyAddAssociationsMixin<GameInstance, GameInstance['id']>;
    addGame: Sequelize.HasManyAddAssociationMixin<GameInstance, GameInstance['id']>;
    createGame: Sequelize.HasManyCreateAssociationMixin<GameAttributes, GameInstance>;
    removeGame: Sequelize.HasManyRemoveAssociationMixin<GameInstance, GameInstance['id']>;
    removeGames: Sequelize.HasManyRemoveAssociationsMixin<GameInstance, GameInstance['id']>;
    hasGame: Sequelize.HasManyHasAssociationMixin<GameInstance, GameInstance['id']>;
    hasGames: Sequelize.HasManyHasAssociationsMixin<GameInstance, GameInstance['id']>;
    countGames: Sequelize.HasManyCountAssociationsMixin;
};

export const UserFactory = (sequelize: Sequelize.Sequelize, dataTypes: Sequelize.DataTypes): Sequelize.Model<UserInstance, UserAttributes> => {
    const attributes: SequelizeAttributes<UserAttributes> = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        apiKey: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        email: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        username: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: dataTypes.STRING,
            unique: false,
            allowNull: false
        }
    };

    const User = sequelize.define<UserInstance, UserAttributes>('User', attributes);

    User.associate = models => {
        User.hasMany(models.Game);
    };

    return User;
};

