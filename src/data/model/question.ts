import * as Sequelize from "sequelize";
import { SequelizeAttributes } from "../typings/sequelizeattrs";

export interface QuestionAttributes {
    id?: number;
    question: string;
    realAnswer: string;
    firstFakeAnswer: string;
    secondFakeAnswer: string;
    thirdFakeAnswer: string;
    confirmed: boolean;
};

export interface QuestionInstance extends Sequelize.Instance<QuestionAttributes>, QuestionAttributes {
};

export const QuestionFactory = (sequelize: Sequelize.Sequelize, dataTypes: Sequelize.DataTypes): Sequelize.Model<QuestionInstance, QuestionAttributes> => {
    const attributes: SequelizeAttributes<QuestionAttributes> = {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        question: {
            type: dataTypes.STRING,
            unique: true,
            allowNull: false
        },
        realAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        firstFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        secondFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        thirdFakeAnswer: {
            type: dataTypes.STRING,
            allowNull: false
        },
        confirmed: {
            type: dataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }

    const Question = sequelize.define<QuestionInstance, QuestionAttributes>('Question', attributes);
    return Question;
};