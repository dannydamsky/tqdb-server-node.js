import * as Sequelize from 'sequelize';
import { DbInterface } from "../typings/dbinterface";
import { UserFactory } from '../model/user';
import { QuestionFactory } from '../model/question';
import { GameFactory } from '../model/game';

export const createModels = (): DbInterface => {
    const sequelize = new Sequelize("tqdb", "root", "_Root123root", {
        host: 'localhost',
        dialect: 'mysql',
        port: 3306,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        logging: false,
        operatorsAliases: false
    });

    const db: DbInterface = {
        sequelize,
        Sequelize,
        User: UserFactory(sequelize, Sequelize),
        Question: QuestionFactory(sequelize, Sequelize),
        Game: GameFactory(sequelize, Sequelize)
      };
      
      Object.values(db).forEach((model: any) => {
        if (model.associate) {
          model.associate(db);
        }
      });

      return db;
}