import * as Sequelize from "sequelize";
import { createModels } from "./config/sequelizeconfig";

const db = createModels();
db.sequelize.sync();

export default db;