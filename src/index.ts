import db from "./data/appdatabase";
import * as express from 'express';
import * as path from "path";
import { PostResource } from "./resource/postresource";
import { Random } from "./util/random";

const app = express();
app.use(express.json());
app.use(express.static(path.join(__dirname, "/../public/view")));
app.use(express.static(path.join(__dirname, "/../public/script")));
app.use(express.static(path.join(__dirname, "/../public/image")));

const addresource = new PostResource(app, db);
addresource.createRouting();

const port = process.env.PORT || 3000;
console.log(`Listening on port ${port}...`);
app.listen(port);