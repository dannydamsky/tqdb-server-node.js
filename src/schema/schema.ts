import * as Joi from 'joi';

export const questionSchema = {
    question: Joi.string().trim().min(3).max(255).required(),
    realAnswer: Joi.string().trim().min(3).max(255).required(),
    firstFakeAnswer: Joi.string().trim().min(3).max(255).required(),
    secondFakeAnswer: Joi.string().trim().min(3).max(255).required(),
    thirdFakeAnswer: Joi.string().trim().min(3).max(255).required()
}

export const userSchema = {
    email: Joi.string().email().required(),
    username: Joi.string().trim().alphanum().min(1).max(20).required(),
    password: Joi.string().alphanum().min(8).max(48).required()
}

export const gameSchema = {
    apiKey: Joi.string().trim().length(32).alphanum().required(),
    score: Joi.number().integer().min(0).max(10).required()
}